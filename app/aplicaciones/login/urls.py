from aplicaciones.login.views import *
from django.urls import  path

from aplicaciones.erp.api.api import CategoryAPIView, DetailCategoryAPIView
from aplicaciones.login.views import LoginForView2


urlpatterns = [
    path('', LoginForView.as_view(), name='login'),
    # FORMA NORMAL DE DESLOGUEO, QUE NOS OFRECE DJANGO
    # path('logout/', LogoutView.as_view(next_page='login'), name='logout'),
#     UTILIZANDO REDIRECT VIEW, HACIENDO LO MISMO, PERO ESCALABLE
    path('logout/', LogoutRedirectView.as_view(), name='logout'),

]