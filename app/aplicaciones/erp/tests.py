import io
import random

from django.test import TestCase

# PERMITE CORRER LA CONFIGURACION DEL SERVIDOR, PARA ASI PODER EJECUTAR EL RESTFRAMEWORK
import os,django


os.environ.setdefault ("DJANGO_SETTINGS_MODULE", "app.settings") # project_name nombre del proyecto
django.setup()
import xlrd
from django.contrib.auth import authenticate, login
from app.wsgi import *
from aplicaciones.erp.models import *
from rest_framework import  serializers
print('HOLA')
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

#Lista
# query = Type.objects.all()
# print(query)

#Insercion
# t = Type()
# t.name = 'Estudiante'
# t.save()
# TRY CATCH
# try:
#     te = Type.objects.get(pk = 4)
#     print(te)
# except Exception as e:
#     print('El error es', e)

#Eliminacion
# teli = Type.objects.get(pk= 1)
# teli.delete()


#Similar a trabajar con like

query = Category.objects.filter()
#
# for i in query:
#     print(i.name)

class PersonaSerializer(serializers.Serializer):
    nombre = serializers.CharField(max_length=20)
    apellidoP = serializers.CharField(max_length=20)
    apellidoM = serializers.CharField(max_length=20)
    telefono = serializers.CharField(max_length=20)
    correo = serializers.EmailField()
    edad = serializers.IntegerField()


class Persona:
    def __init__(self, nombre, apellidoP, apellidoM, telefono, correo, edad):
        self.nombre = nombre
        self.apellidoP = apellidoP
        self.apellidoM = apellidoM
        self.telefono = telefono
        self.correo = correo
        self.edad = edad

persona1 = Persona("Jose","Mayo","Aguilar",993256,"jose@gmail.com",20)

# SERIALIZACION
person_seria = PersonaSerializer(persona1)
print(person_seria.data)
json = JSONRenderer().render(person_seria.data)
print(json)
# DESERIALIZACION
stream = io.BytesIO(json)
data = JSONParser().parse(stream)
print(data)
seria = PersonaSerializer(data=data)
print(seria.is_valid())
print(seria.validated_data)

filePath = '/home/felix/Escritorio/django-apps/django-0/app/static/img/Datos.xlsx'
openFile = xlrd.open_workbook(filePath)
sheet = openFile.sheet_by_name("Hoja1")
print("No de filas", sheet.nrows)
for i in range(sheet.nrows):
    print(sheet.cell_value(i,0), ": ", sheet.cell_value(i,1))

import openpyxl
wb = openpyxl.load_workbook(filePath)

print(wb['Hoja1'])

letters = ['a','b','c','d','e','f','g','h','i','j','k','l','n','m','1','2','3','4','z'
           'x','y']
# for i in range(1,4000):
#     name = ''.join(random.choices(letters,k=5))
#     while Category.objects.filter(name=name).exists():
#         name = ''.join(random.choices(letters, k=5))
#     Category(name=name).save()
#     print('Guardado')

user = authenticate(username='root', password='root')
print(user)
if user is not None:
    # login(user)
    print("HOLA")

data = []
data.append({'data':'NUEVO DATO'})
datos = {
    "travel":data
}

print(datos)