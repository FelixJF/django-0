from datetime import datetime

from django.forms import *
from django.template.defaultfilters import first

from aplicaciones.erp.models import Category, Product, Client


class CategoryForm(ModelForm):
    # Constructor inicial, el cual permite correr el codiigo una vez llamada la instancia
    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        # Para que al primer elemento sin saber su nombre, se le coloque el autofocus
        # Para ellos accedemos al primer elemento de visible_fields, en su atributo field
        # modificamos el widget y atributo
        self.visible_fields()[0].field.widget.attrs['autofocus'] = True

        # LO comentamos para probar la funcionalidad en widget tweaks, pero hacen la misma funcionalidad
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'
    class Meta:
        model = Category
        fields = '__all__'
        exclude = ['user_update', 'user_creation']
        labels = {
            'name': 'Categoria',
            'desc': 'Descripción'
        }
        widgets = {
            'name': TextInput(
                attrs={
                    'placeholder':'Ingrese nombre de categoria'
                }
            ),
            'desc': Textarea(
                attrs={
                    'placeholder': 'Una descripción',
                    'rows':3,
                    'cols':2
                }
            )
        }

    def save(self, commit=True):
        data = {}
        # Obtenemos el form sobre el cual estamos
        form = super()
        # Esto que hacemos aqui, con la finalidad de
        # no escribir mas codigo en la vista, de manera que nuestro
        # formulario pertenenciente a esta entidad
        # haga el trabajo de la validacion-
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    # PERSONALIZA EL FORM QUE ESTAMOS USANDO

    # def clean(self):
    #     cleaned = super().clean()
    #     if len(cleaned['name'])<=50:
    #         # self.add_error('name', 'Requieren mas caracteres')
    #         raise forms.ValidationError('Validacion non field')
    #     print(cleaned)
    #     return cleaned


class ProductForm(ModelForm):
    # Constructor inicial, el cual permite correr el codiigo una vez llamada la instancia
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Para que al primer elemento sin saber su nombre, se le coloque el autofocus
        # Para ellos accedemos al primer elemento de visible_fields, en su atributo field
        # modificamos el widget y atributo
        self.visible_fields()[0].field.widget.attrs['autofocus'] = True

        # LO comentamos para probar la funcionalidad en widget tweaks, pero hacen la misma funcionalidad
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'

    class Meta:
        model = Product
        fields = '__all__'
        labels = {
            'name': 'Nombre del producto',
            'pvp': 'Precio',
            'image': 'Imagen del producto',
            'cate': 'Categoria del producto'
        }
        widgets = {
            'name': TextInput(
                attrs={
                    'placeholder': 'Ingrese nombre del producto'
                }
            ),
            'pvp': TextInput(
                attrs={
                    'placeholder': 'Precio del producto',
                }
            ),

        }

    def save(self, commit=True):
        data = {}
        # Obtenemos el form sobre el cual estamos
        form = super()
        # Esto que hacemos aqui, con la finalidad de
        # no escribir mas codigo en la vista, de manera que nuestro
        # formulario pertenenciente a esta entidad
        # haga el trabajo de la validacion-
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    # PERSONALIZA EL FORM QUE ESTAMOS USANDO

    # def clean(self):
    #     cleaned = super().clean()
    #     if len(cleaned['name'])<=50:
    #         # self.add_error('name', 'Requieren mas caracteres')
    #         raise forms.ValidationError('Validacion non field')
    #     print(cleaned)
    #     return cleaned

class ClientForm(ModelForm):
    # Constructor inicial, el cual permite correr el codiigo una vez llamada la instancia
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Para que al primer elemento sin saber su nombre, se le coloque el autofocus
        # Para ellos accedemos al primer elemento de visible_fields, en su atributo field
        # modificamos el widget y atributo
        self.visible_fields()[0].field.widget.attrs['autofocus'] = True

        # LO comentamos para probar la funcionalidad en widget tweaks, pero hacen la misma funcionalidad
        # for form in self.visible_fields():
        #     form.field.widget.attrs['class'] = 'form-control'
        #     form.field.widget.attrs['autocomplete'] = 'off'

    class Meta:
        model = Client
        fields = '__all__'
        labels = {
            'names': 'Nombres',
            'surnames': 'Apellidos',
            'dni': 'DNI',
            'date_birthday': 'Fecha de nacimiento',
            'address': 'Direccion',
            'gender': 'Sexo'
        }
        widgets = {
            'names': TextInput(
                attrs={
                    'placeholder': 'Ingrese sus nombres',
                    'autocomplete':'Off'
                }
            ),
            'surnames': TextInput(
                attrs={
                    'placeholder': 'Ingrese sus apellidos',
                }
            ),
            'dni': TextInput(
                attrs={
                    'placeholder': 'Ingrese DNI',
                }
            ),
            'date_birthday': DateInput(
                format=('%Y/%m/%d'),
                attrs={
                    'value': datetime.now().strftime('%Y/%m/%d'),
                    'placeholder': 'Seleccione una fecha',
                    'class':'form-control',
                    'type':'date'
                }
            ),
            'address': TextInput(
                attrs={
                    'placeholder': 'Ingrese una dirección',
                }
            ),
            'gender': Select(),
        }
        exclude = ['user_update', 'user_creation']

    def save(self, commit=True):
        data = {}
        # Obtenemos el form sobre el cual estamos
        form = super()
        # Esto que hacemos aqui, con la finalidad de
        # no escribir mas codigo en la vista, de manera que nuestro
        # formulario pertenenciente a esta entidad
        # haga el trabajo de la validacion-
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    # PERSONALIZA EL FORM QUE ESTAMOS USANDO

    # def clean(self):
    #     cleaned = super().clean()
    #     if len(cleaned['name'])<=50:
    #         # self.add_error('name', 'Requieren mas caracteres')
    #         raise forms.ValidationError('Validacion non field')
    #     print(cleaned)
    #     return cleaned

class TestForm(Form):
    # COMO PRECEDEMOS DE UN MODEL, SIEMPRE LE TENEMOS QUE INDICAR AL INICIO EL TIPO DE DATO RELACIONADO A LA BD
    categories = ModelChoiceField(queryset=Category.objects.all(), widget=Select(attrs={
        'class': 'form-control  select2'
    }))
    products = ModelChoiceField(queryset=Product.objects.none(), widget=Select(attrs={
        'class': 'form-control select2'
    }))
    search = ModelChoiceField(queryset=Product.objects.none(),widget=Select(attrs={
        'class':'form-control select2',
        'placeholder':'Ingrese una descripcion'
    }))
    # search = CharField(widget=TextInput(attrs={
    #     'class': 'form-control',
    #     'placeholder': 'Ingrese una descripcion'
    # }))