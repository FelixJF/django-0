from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

from aplicaciones.erp.forms import TestForm
from aplicaciones.erp.models import Product


class TestView(TemplateView):
    template_name = 'test.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        print(request.POST)
        try:
            action = request.POST['action']
            if action == 'producto_id':
                id = request.POST['id']
                data = [{'id': '', 'text': '---------'}]
                for i in Product.objects.filter(cate=id):
                    data.append({'id': i.id, 'text': i.name})
            elif action == 'autocomplete':
                data = []
                print("entra al auto")
                if (request.POST['term']):
                    for i in Product.objects.filter(name__icontains=request.POST['term']):
                        data.append({'id': i.id, 'text': i.name})
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = "NO HAY ACTION"
        #     safe= False, nos permite mandar cualquier objeto, diccionario de datos, que tengan el formato dict
        #     pues este es el unico que permite JsonResponse, en todo caso, nos mandara error-
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = 'Selec anidados | Django'
        context["form"] = TestForm()
        return context
