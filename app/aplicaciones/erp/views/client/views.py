from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from aplicaciones.erp.forms import ClientForm
from aplicaciones.erp.mixins import ValidatePermissionRequiredMixin
from aplicaciones.erp.models import Client, Category
from django.contrib import  messages


class ClientListView(LoginRequiredMixin,ValidatePermissionRequiredMixin,TemplateView):
    template_name = 'client/list.html'
    # LO QUE SIGNIFICA QUE, OBLIGA A QUE EL USUARIO TENGA ESTOS PERMISOS PARA ASI ACCEDER A LA PAGINA
    # Es decir, si el usuario entra en esa vista, no es que si no tiene el permiso de ver, no pueda entrar
    # Si no que yo aqui agrego los permisos a los cuales es necesario que el usuario tenga acceso
    #
    permission_required = ('erp.add_client','erp.change_client')
    @method_decorator(csrf_exempt)
    # @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        print(request.POST)
        try:
            action = request.POST['action']
            if action == 'getdata':
                data = []
                for i in Client.objects.all():
                    data.append(i.toJSON())
            elif action == 'add':
                cli = Client()
                cli.names = request.POST['names']
                cli.surnames = request.POST['surnames']
                cli.dni = request.POST['dni']
                cli.date_birthday = request.POST['date_birthday']
                cli.address = request.POST['address']
                cli.gender = request.POST['gender']
                cli.save()
            elif action == 'edit':
                cli = Client.objects.get(pk=request.POST['id'])
                cli.names = request.POST['names']
                cli.surnames = request.POST['surnames']
                cli.dni = request.POST['dni']
                cli.date_birthday = request.POST['date_birthday']
                cli.address = request.POST['address']
                cli.gender = request.POST['gender']
                cli.save()
            elif action == 'del':
                cli = Client.objects.get(pk=request.POST['id'])
                cli.delete()
            elif action == 'autocomplete':
                data = []
                # PARA  EL AUTOCOMPLETE, DEBE IR COMO JSON, MAS QUE NADA  EL VALUE, Q ES EL VALOR Q VA A TOMAR
                for i in Client.objects.filter(names__icontains=request.POST['term']):
                    item = i.toJSON()
                    item['value'] = i.names
                    data.append(item)
            else:
                data['error'] = 'Ha ocurrido un error'

        except Exception as e:
            data['error'] = "NO HAY ACTION"
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super(ClientListView, self).get_context_data(**kwargs)
        # context['clientes'] = Client.objects.all()
        context['title'] = "Registros"
        context['url_pattern'] = reverse_lazy('erp:client_list')
        context['form'] = ClientForm()
        return context
