from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, FormView
from django.urls import reverse_lazy
from aplicaciones.erp.forms import CategoryForm
from aplicaciones.erp.mixins import IsSuperuserMixin, ValidatePermissionRequiredMixin
from aplicaciones.erp.models import Category


@method_decorator(csrf_exempt)
def category_list(request):
    print(request.POST)
    print(request.POST.getlist('download_inv[0]'))
    return JsonResponse(request.POST)


# Hace lo mismo que category list, pero de una forma mas automatica, sin embargo
# la data que mandamos no, es por eso que debemos sobreescribir algunos metodos para lograrlo

class CategoryListView(LoginRequiredMixin,ValidatePermissionRequiredMixin, ListView):
    model = Category
    template_name = 'category/list.html'
    permission_required = 'erp.add_sale'

    @method_decorator(csrf_exempt)
    # @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    # ESTE POST NO LO ESTAMOS USANDO
    def post(self, request, *args, **kwargs):
        data = {}
        print(request.POST)
        try:
            action = request.POST['action']
            # Vamos a consultar todos los datos, y los vamos a ir guardando en un array
            # iteramos esa consulta, y con el toJSON de esa entidad,
            # podremos conseguir en formato JSON cada uno de los registros-
            if action == 'getdata':
                # Una coleccion
                data = []
                for i in Category.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'

        except Exception as e:
            data['error'] = "NO HAY ACTION"
        #     safe= False, nos permite mandar cualquier objeto, diccionario de datos, que tengan el formato dict
        #     pues este es el unico que permite JsonResponse, en todo caso, nos mandara error-
        return JsonResponse(data, safe=False)

    # Al sobreescribir este metodo, le pasamos datos en un diccionario que podemos usar en el template
    # De esta forma, se convierte un poco dinamico, pues cambia dependiendo del list donde nos encontremos-
    def get_context_data(self, **kwargs):
        context = super(CategoryListView, self).get_context_data(**kwargs)
        context['categorias'] = Category.objects.all()
        context['title'] = "Registros"
        context['url_form'] = reverse_lazy('erp:category_create')
        context['url_pattern'] = reverse_lazy('erp:category_list')

        return context


class CategoryCreateView(ValidatePermissionRequiredMixin, CreateView):
    permission_required = 'erp.add_category'
    url_redirect = reverse_lazy('erp:category_list')
    model = Category
    form_class = CategoryForm
    template_name = 'category/create.html'
    # Colocale erp:
    # Ya que son la urls que estan dentro de la app erp
    success_url = reverse_lazy('erp:category_list')

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    # """
    #     # Sobreescribimos el metodo post, para asi poder revisar los errores (mas que nada comprender, ya que no es necesario)  ,
    #     # como el colocar la misma palabra para nombre, en caso que este se repita
    #     def post(self, request, *args, **kwargs):
    #         print(request.POST)
    #         # De la data que mandamos,paso primero por esta vista de clase, la cual le mando el
    #         # formulario que creamos con ModelForm
    #         # Por lo tanto, al guardar volvera a entrar se ira a su metodo post, pero ahora ya lo
    #         # estamos sobreescribiendo,y vamos a capturar validaciones
    #         # Para eso, capturamos el post y le mandamos la data a clase del formulario
    #         # Posteriormente preguntamos si es valido, en ese caso nos retorne a la pagina donde esta el success url-
    #         form = CategoryForm(request.POST)
    #         if form.is_valid():
    #             form.save()
    #             return HttpResponseRedirect(self.success_url)
    #         # Por otro lado, en caso contrario, deberemos renderizar nuevamente el template
    #         # Pero sera necesario mandarle los datos del content text-
    #         self.object = None
    #         context = self.get_context_data(**kwargs)
    #         context['form']= form
    #
    #         return render(request, self.template_name,context)
    # """
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CategoryCreateView, self).get_context_data(**kwargs)
        context['title'] = "Crear categoria"
        context['list_url'] = self.success_url
        context['action'] = 'add'

        return context


class CategoryUpdateView(UpdateView):
    model = Category
    form_class = CategoryForm
    template_name = 'category/create.html'
    # Colocale erp:
    # Ya que son la urls que estan dentro de la app erp
    success_url = reverse_lazy('erp:category_list')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'Sin opcion'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Edicion de categoria"
        context['list_url'] = self.success_url
        context['action'] = 'edit'

        return context


class CategoryDeleteView(DeleteView):
    model = Category
    template_name = 'category/delete.html'
    success_url = reverse_lazy('erp:category_list')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = " ¿Seguro que quiere borrar el siguiente registro?"
        context['list_url'] = self.success_url
        context['name'] = self.get_object().name

        return context


class CategoryFormView(FormView):
    # Solo le pasamos el form, no el modelo, asi trabaja esta vista de clase
    form_class = CategoryForm
    template_name = 'category/create.html'
    success_url = reverse_lazy('erp:category_list')

    def form_valid(self, form):
        print(form.is_valid())
        print(form)
        return super().form_valid(form)

    def form_invalid(self, form):
        print(form.errors)
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "FORM | CATEGORIA"
        context['list_url'] = self.success_url
        context['action'] = 'add'

        return context
