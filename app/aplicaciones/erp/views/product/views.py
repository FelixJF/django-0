from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, FormView
from django.urls import reverse_lazy
from aplicaciones.erp.forms import CategoryForm, ProductForm
from aplicaciones.erp.models import Category, Product


# Hace lo mismo que category list, pero de una forma mas automatica, sin embargo
# la data que mandamos no, es por eso que debemos sobreescribir algunos metodos para lograrlo
class ProductListView(ListView):
    model = Product
    template_name = 'product/list.html'

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):

        return super().dispatch(request, *args, **kwargs)

    # ESTE POST LO USAMOS, YA QUE EL JS AL CORRER LA VISTA REQIERE DE LOS REGISTROS TOTALES POR AJAX
    # YA NO LO USAMOS AQUI CON EL PRODUCTO :((
    def post(self, request, *args, **kwargs):
        data = {}
        print(request.POST)
        try:
            action = request.POST['action']
            # Vamos a consultar todos los datos, y los vamos a ir guardando en un array
            # iteramos esa consulta, y con el toJSON de esa entidad,
            # podremos conseguir en formato JSON cada uno de los registros-
            if action == 'getdata':
                # Una coleccion
                data = []
                for i in Product.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'

        except Exception as e:
            data['error'] = "NO HAY ACTION"
        #     safe= False, nos permite mandar cualquier objeto, diccionario de datos, que tengan el formato dict
        #     pues este es el unico que permite JsonResponse, en todo caso, nos mandara error-
        return JsonResponse(data, safe=False)

    # Al sobreescribir este metodo, le pasamos datos en un diccionario que podemos usar en el template
    # De esta forma, se convierte un poco dinamico, pues cambia dependiendo del list donde nos encontremos-
    def get_context_data(self, **kwargs):
        context = super(ProductListView, self).get_context_data(**kwargs)
        context['title'] = "Registros"
        context['url_form'] = reverse_lazy('erp:product_create')
        context['url_pattern'] = reverse_lazy('erp:product_list')

        return context


class ProductCreateView(CreateView):
    model = Product
    form_class = ProductForm
    template_name = 'product/create.html'
    # Colocale erp:
    # Ya que son la urls que estan dentro de la app erp
    success_url = reverse_lazy('erp:product_list')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'add':
                # ESTAS DOS LINEAS HACEN LO MISMO, RECOGEN LOS DATOS ENVIANDOS DEL FORM
                # form = CategoryForm(request.POST)
                form = self.get_form()
                # Usamos el metodo asi, ya que lo sobrescribimos en su clase, de manera que lo estamos
                # poniendo a trabajar, y aqui en el controller no requeir de tanto codigo-
                # Afuerza requieres ejecutar el is_valid antes para que podamos ejecutar save()
                # aqui lo estamos haciendo al sobreescribir el save del form-
                data = form.save()
                # if form.is_valid():
                #     form.save()
                # else:
                #     data['error'] = form.errors
            else:
                data['error'] = 'Sin opcion'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super(ProductCreateView, self).get_context_data(**kwargs)
        context['title'] = "Crear producto"
        context['list_url'] = self.success_url
        context['action'] = 'add'

        return context


class ProductUpdateView(UpdateView):
    model = Product
    form_class = ProductForm
    template_name = 'product/create.html'
    # Colocale erp:
    # Ya que son la urls que estan dentro de la app erp
    success_url = reverse_lazy('erp:product_list')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'edit':
                form = self.get_form()
                data = form.save()
            else:
                data['error'] = 'Sin opcion'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Edición de producto"
        context['list_url'] = self.success_url
        context['action'] = 'edit'

        return context


class ProductDeleteView(DeleteView):
    model = Product
    template_name = 'product/delete.html'
    success_url = reverse_lazy('erp:product_list')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            self.object.delete()
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = " ¿Seguro que quiere borrar el siguiente registro?"
        context['list_url'] = self.success_url
        context['name'] = self.get_object().name

        return context
