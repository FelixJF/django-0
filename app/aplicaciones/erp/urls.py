from django.urls import  path

from aplicaciones.erp.api.api import CategoryAPIView, DetailCategoryAPIView
from aplicaciones.erp.views.category.views import  *
from aplicaciones.erp.views.client.views import ClientListView
from aplicaciones.erp.views.dashboard.views import DashboardView
from aplicaciones.erp.views.product.views import ProductListView, ProductCreateView, ProductUpdateView, \
    ProductDeleteView
from aplicaciones.erp.views.test.views import TestView

app_name = 'erp'
urlpatterns = [


    path('category/prueba', category_list, name='category'),

    path('category/list', CategoryListView.as_view(), name='category_list'),
    path('category/create/', CategoryCreateView.as_view(), name='category_create'),
    path('category/update/<int:pk>/', CategoryUpdateView.as_view(), name='category_update'),
    path('category/delete/<int:pk>/', CategoryDeleteView.as_view(), name='category_delete'),
    # PRODUCT
    path('product/list', ProductListView.as_view(), name='product_list'),
    path('product/create/', ProductCreateView.as_view(), name='product_create'),
    path('product/update/<int:pk>', ProductUpdateView.as_view(), name='product_update'),
    path('product/delete/<int:pk>', ProductDeleteView.as_view(), name='product_delete'),
    # CLIENT
    path('client/list', ClientListView.as_view(), name='client_list'),

    # PRUEBAS APIS
    path('categoria/', CategoryAPIView.as_view(),name="category_api" ),
    path('categoria/detail/',DetailCategoryAPIView.as_view(),name="category_detail_api"),



    # test
    path('test/', TestView.as_view(), name="test_view"),

    # FORM VIEWS
    path('category/form/',CategoryFormView.as_view(),name="category_form"),

    # DASHBOAR
    path('dashboard/',DashboardView.as_view(),name="dashboard")
]

