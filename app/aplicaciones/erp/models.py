from crum import get_current_user
from django.db import models
from datetime import datetime

from aplicaciones.models import BaseModel
from app.settings import MEDIA_ROOT, MEDIA_URL, STATIC_URL
from django.forms import model_to_dict

from aplicaciones.erp.choices import gender_choices


class Category(BaseModel):
    name = models.CharField(max_length=150, verbose_name='Nombre', unique=True)
    desc = models.CharField(max_length=150, verbose_name='Descripcion')

    def __str__(self):
        return '{}'.format(self.name)

    def toJSON(self):
        # Podemos excluir parametros
        # Nos devuelve en formato json la informacon del registro, algo como los serializer:)
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'
        ordering = ['id']

    #      AUDITORIA DE QUIEN CREA Y EDITA ESTE REGISTRO
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        user = get_current_user()
        if user and not user.pk:
            user = None
        if not self.pk:
            self.user_creation = user
        self.user_update = user
        super(Category, self).save()


class Product(models.Model):
    name = models.CharField(max_length=150, verbose_name='Nombre', unique=True)
    cate = models.ForeignKey(Category, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='product/%Y/%m/%d', null=True, blank=True)
    pvp = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)

    def __str__(self):
        return self.name

    def get_name_cat(self):
        return self.cate.name

    def get_image(self):
        if self.image:
            return '{}{}'.format(MEDIA_URL, self.image)
        return '{}{}'.format(STATIC_URL, 'img/vacio.png')

    class Meta:
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'
        ordering = ['id']


class Client(models.Model):
    names = models.CharField(max_length=150, verbose_name='Nombres')
    surnames = models.CharField(max_length=150, verbose_name='Apellidos')
    dni = models.CharField(max_length=10, unique=True, verbose_name='Dni')
    date_birthday = models.DateField(default=datetime.now, verbose_name='Fecha de nacimiento')
    address = models.CharField(max_length=150, null=True, blank=True, verbose_name='Dirección')
    gender = models.CharField(max_length=10, choices=gender_choices, default='male', verbose_name='Sexo')

    def __str__(self):
        return self.names
    def toJSON(self):
        item = model_to_dict(self)
        # RETORNA EL VALOR QUE SE ENCUENTRA EN LOS CHOICES
        # ESTAMOS SOBRESESCRIBIENDO EL VALOR QUE LES ASIGNA EL model_to_dict
        # OCUPAMOS CAMBIAR SU VALOR A UNO QUE PUEDA SER LEGIBLE EN JSON
        item['gender'] = {'id':self.gender, 'name':self.get_gender_display()}
        item['date_birthday'] = self.date_birthday.strftime('%Y-%m-%d')
        return item

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'
        ordering = ['id']


class Sale(models.Model):
    cli = models.ForeignKey(Client, on_delete=models.CASCADE)
    date_joined = models.DateField(default=datetime.now)
    subtotal = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    iva = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    total = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)

    def __str__(self):
        return self.cli.names

    class Meta:
        verbose_name = 'Venta'
        verbose_name_plural = 'Ventas'
        ordering = ['id']


class DetSale(models.Model):
    sale = models.ForeignKey(Sale, on_delete=models.CASCADE)
    prod = models.ForeignKey(Product, on_delete=models.CASCADE)
    price = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)
    cant = models.IntegerField(default=0)
    subtotal = models.DecimalField(default=0.00, max_digits=9, decimal_places=2)

    def __str__(self):
        return self.prod.name

    class Meta:
        verbose_name = 'Detalle de Venta'
        verbose_name_plural = 'Detalle de Ventas'
        ordering = ['id']

#
# class Employee(models.Model):
#     categ = models.ManyToManyField(Category)
#     type = models.ForeignKey(Type, on_delete=models.CASCADE)
#     names = models.CharField(max_length=150, verbose_name='Nombres')
#     dni = models.CharField(max_length=10, verbose_name='DNI', unique=True)
#     date_joined = models.DateField(default=datetime.now, verbose_name='Fecha de registro')
#     date_creation = models.DateTimeField(auto_now=True)
#     date_updated = models.DateTimeField(auto_now=True)
#     age = models.PositiveIntegerField(default=0)
#     salary = models.DecimalField(default=0.00, decimal_places=6, max_digits=10)
#     state = models.BooleanField(default=True)
#     avatar = models.ImageField(upload_to='avatar/%Y/%m/%d', null=True, blank=True)
#     cvitae = models.FileField(upload_to='cvitae/%Y/%m/%d', null=True, blank=True)
#     #gender = models.CharField(max_length=50)
#     #number = models.CharField(max_length=10)
#
#
#     def __str__(self):
#         return self.names
#     class Meta:
#         verbose_name = 'Empleado'
#         verbose_name_plural = 'Empleados'
#         db_table = 'empleado'
#         ordering = ['id']
#
