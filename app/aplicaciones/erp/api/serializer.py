from rest_framework import  serializers
from aplicaciones.erp.models import Category

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
        