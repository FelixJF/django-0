from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from aplicaciones.erp.api.serializer import CategorySerializer

from aplicaciones.erp.models import *


class CategoryAPIView(APIView):
    def get(self, request):
        categories = Category.objects.all()
        categories_serializer = CategorySerializer(categories, many=True)
        return Response(categories_serializer.data)

    def post(self, request):
        category = CategorySerializer(data=request.data)
        if category.is_valid():
            category.save()
            return Response(category.data)
        return Response(category.errors)


class DetailCategoryAPIView(APIView):
    def get(self, request):
        # De esta manera no usamos el trycatch para saber si existe o no el registro de la categoria
        # **Le podemos mandar data del pk, aunque el metodo sea get, falta analizar mas aqui, de esta forma recuperamos la info
        pk = request.data['pk']
        category = Category.objects.filter(id=pk).first()
        category_serializer = CategorySerializer(category)
        return Response(category_serializer.data)

    def post(self, request):
        # Es necesario tener como parametro requerido el pk de la categoria a actualizar, entra en el try para +
        # asi validar, y en caso contrario, que nos retorne el error y no prosiga
        # Al quitarlo, va directmente al guardado, pues para el serializer si cumple con los atributos que posee el modelo-
        try:
            pk = request.data['pk']
        except Exception as e:
            return Response(data={
                "status": status.HTTP_400_BAD_REQUEST,
                "message": "Parametro requerido" + str(e)
            })
        category = Category.objects.filter(id=pk).first()
        # Para update, le pasamos la instancia que se encuentra con la consulta
        # y como 2do parametro, la data del request que tiene que llegar por PUT o POST
        category_serializer = CategorySerializer(category, data=request.data)
        # Como si hubo el pk, resta validar si de los campos que estan llegando, cumplen con las restricciones
        # colocadas en el modelo, en caso contrario, retorna el error y no graba nada-
        if category_serializer.is_valid():
            category_serializer.save()
            return Response(category_serializer.data)
        return Response(category_serializer.errors)
    def delete(self,request):
        try:
            pk = request.data['pk']
        except Exception as e:
            return Response(data={
                "status": status.HTTP_404_NOT_FOUND,
                "message": "Parametro requerido" + str(e)
            })
        if pk:
            try:
                Category.objects.get(pk=pk)
            except Category.DoesNotExist:
                return Response(data={
                    "status": status.HTTP_400_BAD_REQUEST,
                    "success": False,
                    "message": "Categoria no existe"
                })
            category = Category.objects.get(pk=pk)
            category.delete()
            return Response(data="Eliminado con exito")




