$(function (){
    //Construccion de un datatable
    $('#example').DataTable({
        responsive:true,
        autoWidth:false,
        destroy:true,
        //Parametro para poder cargar mas de 4000mil registros
        deferRender: true,
        //Abrimos la peticion ajax, el cual nos va a retornar una data
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            //Le mandaremos por post, la accion que queremos realizer
            data: {
                'action':'getdata'
            },
            dataSrc: ""
        },
        //Establecemos la columnas que van a venir y que pocision van a tomar
        columns: [
            {"data": "id"},
            {"data": "name"},
            {"data": "desc"},
            {"data": "botones"}
        ],
        //Esto nos sirve para configurar a un columna en especidifo
        // de la cual queremos que tenga cierto comportamiento-
        columnDefs: [
            {
                targets: [-1],
                class : 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    let buttons = '<a href="/erp/category/update/'+row.id +'/" class="btn  btn-warning btn-xs"><i class="fa fa-edit"></i></a>';
                    buttons +='<a href="/erp/category/delete/'+row.id +'/" class="btn  btn-danger btn-xs"><i class="fa fa-trash"></i></a>';
                    return buttons
                }
            },
        ],
        _fnInitComplete: function (settings, json){

        }
    })
})