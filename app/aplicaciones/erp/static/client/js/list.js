let tblCLient;

function getData() {

    //Construccion de un datatable
    tblCLient = $('#example').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        //Parametro para poder cargar mas de 4000mil registros
        deferRender: true,
        //Abrimos la peticion ajax, el cual nos va a retornar una data
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            //Le mandaremos por post, la accion que queremos realizer
            data: {
                'action': 'getdata'
            },
            dataSrc: ""
        },
        //Establecemos la columnas que van a venir y que pocision van a tomar
        //ESTOS DATOS SON EL DICCIONARIO JSON(en la vista, el toJSON) QUE VIENE POR CADA REGISTRO
        columns: [
            {"data": "id"},
            {"data": "names"},
            {"data": "surnames"},
            {"data": "dni"},
            {"data": "date_birthday"},
            {"data": "address"},
            {"data": "gender.name"},
            {"data": "Opciones"},
        ],
        //Esto nos sirve para configurar a un columna en especidifo
        // de la cual queremos que tenga cierto comportamiento-
        columnDefs: [
            {
                targets: [-1],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                    let buttons = '<a rel="edit" class="btn edit btn-warning btn-xs"><i class="fa fa-edit"></i></a>';
                    buttons += '<a rel="del" class="btn del btn-danger btn-xs"><i class="fa fa-trash"></i></a>';
                    return buttons
                }
            },
        ],
        _fnInitComplete: function (settings, json) {

        }
    })
}

$(function () {
    modelTitle = $('#title-modal')

    getData()
    $('form').on('submit', function (e) {
        e.preventDefault()
        let params = new FormData(this);
        // console.log(params)
        let msj = '';
        let notificacion = null;
        let url = window.location.pathname
        msj = 'Registro guardado';
        guardar_registro(url, params, function () {
            getData()
            $('#myModalClient').modal('hide')
            // $(this)[0].reset();
        }, msj);
    });

    $('#example tbody').on('click', 'a', function () {
        if (this.classList.contains('edit')) {
            // let data = tblCLient.row($(this).parents('tr')).data();
            //OCUPAREMOS ESTE PARA QUE FUNCIONE IGUAL CUANDO SEA RESPONSIVO
            let tr = tblCLient.cell($(this).closest('td, li')).index();
            //LA DATA QUE GUARDA, ES EL JSON QUE MANDAMOS DEL toJSON en su MODEL
            let data = tblCLient.row(tr.row).data()
            console.log(data)
            $('input[name="names"]').val(data.names)
            $('input[name="surnames"]').val(data.surnames)
            $('input[name="dni"]').val(data.dni)
            $('input[name="date_birthday"]').val(data.date_birthday)
            $('input[name="address"]').val(data.address)
            $('input[name="gender"]').val(data.gender.id)
            $('input[name="action"]').val('edit');
            $('input[name="id"]').val(data.id);
            // $('#form').get(0).reset();
            modelTitle.find('span').text("Editar cliente")
            modelTitle.find('i').removeClass().addClass('fa fa-edit mr-2')
            $('#myModalClient').modal('show');
        } else if (this.classList.contains('del')) {
            // let data = tblCLient.row($(this).parents('tr')).data();
            //OCUPAREMOS ESTE PARA QUE FUNCIONE IGUAL CUANDO SEA RESPONSIVO
            let tr = tblCLient.cell($(this).closest('td, li')).index();
            //LA DATA QUE GUARDA, ES EL JSON QUE MANDAMOS DEL toJSON en su MODEL
            let data = tblCLient.row(tr.row).data()

            let params = new FormData();
            params.append('id',data.id)
            params.append('action','del')

            let msj = "¿Seguro que quiere elimina el cliente";
            let notificacion = "¿Esta seguro de eliminar el cliente?";
            let url = window.location.pathname
            msj = 'Registro eliminado';
            ajax_confirm(url, params, function () {
                getData()
            }, msj, notificacion);
        }

    })

    $('.btnAdd').on('click', function () {
        $('input[name="action"]').val('add');
        // $('#form').get(0).reset();
        modelTitle.find('span').text("Registrar cliente")
        modelTitle.find('i').removeClass().addClass('fa fa-plus-square mr-2')
        $('#form')[0].reset();
        $('#myModalClient').modal('show');
    })
})

