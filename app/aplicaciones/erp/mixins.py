from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.contrib import  messages


class IsSuperuserMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        return redirect('erp:dashboard')


# Validaremos los permisos, con la intencion de personalisar nuestros MIXIN
class ValidatePermissionRequiredMixin(object):
    permission_required = ''
    url_redirect = None
    # Deberemos obtener de que modo llega nuestro permisos, en caso de ser 1, habra que convertirlo a una tupla, si no, pues entonces ya es
    # To-do para poder ejecutar el hasperms()
    def get_permis(self):
        if isinstance(self.permission_required, str):
            perms = (self.permission_required,)
        else:
            perms = self.permission_required
        return perms
    # EN caso de no pasarle un url por la variable url_redirect, nos devolvera al login, el cual nos devolera al dashboard, ya que si estamos logueados
    def get_url_redirect(self):
        if self.url_redirect is None:
            return reverse_lazy('login')
        return self.url_redirect
    def dispatch(self, request, *args, **kwargs):
        # De nuestro usuario, ejecutamos has_perms, pero es necesario mandarle la tupla de permisos.
        if request.user.has_perms(self.get_permis()):
            return super().dispatch(request, *args, **kwargs)
        messages.error(request,'No tiene permiso para ingresar a este modulo:(')
        return redirect(self.get_url_redirect())

