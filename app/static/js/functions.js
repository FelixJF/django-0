function mensaje_error(obj) {
    let html = '<ul class="list-group  d-block">'

    console.log(obj)
    if (typeof (obj) === 'object') {
        $.each(obj, function (key, value) {
            // html += '<li class="list-group  d-block"><strong>' + key.toUpperCase() + '</strong>: ' + value + '</li>';
            html += '<li class="list-group  d-block">' + value + '</li>';
        });
        html += '</ul>';
    } else {
        html += '<li class="list-group  d-block">' + obj + '</ul>'
    }
    console.log(html);
    Swal.fire(
        'Error',
        html,
        'error'
    );
}

//DIALOGO DE CONFIRMACION GENERICO
//PUES PODRA SERVIR PARA CUALQUIER TIPO DE CONFIRMACION, SE REQUIERE DELA URL A DONDE QUEREMOS
//QUE VAYA LA INFORMACION, LOS PARAMETROS, Y SI TODO SALIO BIEN, ENTONCES LLAMAMMOS A LA FUNCION
//QUE EL PROGRAMADOR HAYA ESTABLECIDO
function ajax_confirm(url, parameters, callback, msj = null, notificacion) {
    $.confirm({
        title: 'Confirmar',
        content: notificacion,
        icon: 'fa fa-exclamation-triangle',
        buttons: {
            success: {
                text: "Si",
                btnClass: 'btn-success',
                action: function () {
                    //AJAX SE EJECUTA SI SE SE SELECCIONO EL SI COMO OPCION
                    //
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: parameters,
                        dataType: 'json',
                        //COMENTADO PARS QUE FUNCIONE EL DELETE, YA QUE FALLA AL MANDAR EL CSRF TOKEN
                        processData: false,
                        contentType: false
                    }).done(function (data) {
                        if (data.error) {
                            mensaje_error(data.error)
                        } else {
                            if (msj == null) {
                                msj = "Accion ejecutada"
                            }
                            Swal.fire({
                                title: msj,
                                icon: 'success'

                            }).then(function () {
                                callback()
                            });
                        }
                    }).fail(function (data) {
                    }).always(function (data) {

                    })
                }
            },
            danger: {
                text: "No",
                btnClass: 'btn-red ',
            },
        }
    });

}

function guardar_registro(url, parameters, callback, msj = null) {

    $.ajax({
        url: url,
        type: 'POST',
        data: parameters,
        dataType: 'json',
        //CON ESTO DEJA DEJA FUNCIONAR EL NORMAL
        processData: false,
        contentType: false
    }).done(function (data) {
        if (data.error) {
            mensaje_error(data.error)
        } else {
            if (msj == null) {
                msj = "Accion ejecutada"
            }
            Swal.fire({
                title: msj,
                icon: 'success'

            }).then(function () {
                callback()
            });
        }
    }).fail(function (data) {
    }).always(function (data) {

    })
}


// let data = [
//     {
//         id:1,
//         carrera:"educacion",
//         documento:"documento titulo"
//     },
//     {
//         id:1,
//         carrera:"curso2",
//         documento:"documento titulo"
//     },
//     {
//         id:1,
//         carrera:"curso2",
//         documento:"documento titulo"
//     },
//     {
//         id:1,
//         carrera:"curso2",
//         documento:"documento titulo"
//     },
//     {
//         id:2,
//         carrera:"educacion",
//         documento:"documento intermedio"
//     },
//     {
//         id:3,
//         carrera:"educacion",
//         documento:"documento final "
//     },
//       {
//         id:4,
//         carrera:"curso",
//         documento:"documento curso "
//     },
//
// ];
// let nuevaData = [];
// let compruebaEntidad = []
//
// for (var i in data){
//     entidad = data[i].carrera;
//     if (!compruebaEntidad.includes(entidad)){
//         compruebaEntidad.push(entidad)
//         elemento = []
//         indice = entidad
//         datos = []
//         for (var e in data){
//             if(entidad == data[e].carrera){
//                 datos.push(data[e].documento)
//             }
//         }
//         elemento.push(indice)
//         elemento.push(datos)
//         nuevaData.push(elemento)
//     }
//
// }
// console.log(nuevaData)
//
